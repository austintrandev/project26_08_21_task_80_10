package com.shoppizza365.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="customers")
public class CCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="address")
	private String Address;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="postal_code")
	private String postalCode;
	
	@Column(name="country")
	private String country;
	
	@Column(name="sales_rep_employee_number")
	private int salesRepEmployeeNumber;
	
	@Column(name="credit_limit")
	private int creditLimit;
	
	 @OneToMany(targetEntity = CPayment.class, cascade = CascadeType.ALL)
	 @JoinColumn(name = "customer_id")
	 private List<CPayment> payment;
	 
	 @OneToMany(targetEntity = COrder.class, cascade = CascadeType.ALL)
	 @JoinColumn(name = "customer_id")
	 private List<COrder> order;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}

	public void setSalesRepEmployeeNumber(int salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}

	public int getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(int creditLimit) {
		this.creditLimit = creditLimit;
	}

	public List<CPayment> getPayment() {
		return payment;
	}

	public void setPayment(List<CPayment> payment) {
		this.payment = payment;
	}

	public List<COrder> getOrder() {
		return order;
	}

	public void setOrder(List<COrder> order) {
		this.order = order;
	}
}
